package ece651.sp22.qg45.battleship;

/**
 * Check ship for its proper orientation
 */
public class ShipOrientationChecker extends OrientationRuleChecker {

    public ShipOrientationChecker(OrientationRuleChecker next) {
        super(next);
    }

    /**
     * Check if the ship is following its own rotation rule.
     */
    @Override
    protected boolean checkMyRule(String name, char orientation) {
        // for rectangle ship
        if (name.equals("Submarine") || name.equals("Destroyer")) {
            if (orientation == 'V' || orientation == 'H') {
                return true;
            }
            return false;
        // not rectangle ship
        } else {
            if (orientation == 'U' || orientation == 'R' || orientation == 'D' || orientation == 'L') {
                return true;
            }
            return false;
        }
    }
    
    
}
