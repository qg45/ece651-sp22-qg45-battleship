package ece651.sp22.qg45.battleship;

/**
 * Placement class which stores coordinate and orientation.
 */
public class Placement {
    private final Coordinate coordinate;
    private final char orientation;

    /**
     * Constructs placement object given coordinate object and orientation
     * character.
     * 
     * @param coordinate
     * @throws IllegalArgumentException if the orientation character is invalid
     */
    public Placement(Coordinate coordinate, char orientation) {
        if (orientation != 'v' && orientation != 'V'
                && orientation != 'h' && orientation != 'H'
                && orientation != 'U' && orientation != 'R'
                && orientation != 'D' && orientation != 'L'
                && orientation != 'u' && orientation != 'r'
                && orientation != 'd' && orientation != 'l') {
            throw new IllegalArgumentException(orientation + " is invalid letter for orientation");
        }
        this.coordinate = coordinate;
        this.orientation = Character.toUpperCase(orientation);
    }

    /**
     * Constructs placement object given a string of placement information.
     * 
     * @param placement
     * @throws IllegalArgumentException if the input is invalid
     */
    public Placement(String placement) {
        if (placement.length() != 3) {
            throw new IllegalArgumentException("Input placement string's length is not 3");
        }

        this.coordinate = new Coordinate(placement.substring(0, 2));
        char orienLetter = placement.charAt(2);

        if (orienLetter != 'v' && orienLetter != 'V'
                && orienLetter != 'h' && orienLetter != 'H'
                && orienLetter != 'U' && orienLetter != 'R'
                && orienLetter != 'D' && orienLetter != 'L'
                && orienLetter != 'u' && orienLetter != 'r'
                && orienLetter != 'd' && orienLetter != 'l') {
            throw new IllegalArgumentException(orienLetter + " is invalid letter for orientation");
        }
        this.orientation = Character.toUpperCase(orienLetter);
    }

    /**
     * Returns the coordinate information in placement.
     * 
     * @return Coordinate
     */
    public Coordinate getCoordinate() {
        return coordinate;
    }

    /**
     * Returns the orientation information in placement.
     * 
     * @return char
     */
    public char getOrientation() {
        return orientation;
    }

    /**
     * Overriden toString()
     */
    @Override
    public String toString() {
        return "Placement [coordinate=" + coordinate + ", orientation=" + orientation + "]";
    }

    /**
     * Overriden hashcode()
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Overriden equals()
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Placement other = (Placement) obj;
        if (!coordinate.equals(other.coordinate))
            return false;
        if (orientation != other.orientation)
            return false;
        return true;
    }

}
