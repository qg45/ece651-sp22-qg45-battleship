package ece651.sp22.qg45.battleship;

/**
 * BattleShipBoard class is used to contain the information of playing
 * board for the two player.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
  private final int width;
  private final int height;
  private final ArrayList<Ship<T>> myShips;
  private final PlacementRuleChecker<T> placementChecker;
  private final OrientationRuleChecker orientationRuleChecker;
  private final CompletionRules<T> completionRules;
  HashSet<Coordinate> enemyMisses;
  HashMap<Coordinate, Ship<T>> enemyHits;
  final T missInfo;

  /**
   * Constructs a BattleShipBoard with the specified width
   * height, and the placementChecker
   * 
   * @param width
   * @param height
   * @param placementChecker
   * @param missInfo
   */
  public BattleShipBoard(int width, int height, PlacementRuleChecker<T> placementChecker, T missInfo,
      CompletionRules<T> completionRules, OrientationRuleChecker orientationRuleChecker) {
    if (width <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
    }
    if (height <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
    }
    this.width = width;
    this.height = height;
    this.myShips = new ArrayList<>();
    this.placementChecker = placementChecker;
    this.enemyMisses = new HashSet<>();
    this.enemyHits = new HashMap<>();
    this.missInfo = missInfo;
    this.completionRules = completionRules;
    this.orientationRuleChecker = orientationRuleChecker;
  }

  /**
   * Constructs a BattleShipBoard with the specified width
   * and height
   * 
   * @param width  is the width of the newly constructed board.
   * @param height is the height of the newly constructed board.
   * @throws IllegalArgumentException if the width or height are less than or
   *                                  equal to zero.
   */
  public BattleShipBoard(int width, int height, T missInfo) {
    this(width, height, new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null)), missInfo,
        new AllSunkRule<>(null), new ShipOrientationChecker(null));
  }

  /**
   * Returns the filed width of BattleShipBoard object
   * 
   * @return int width of BattleShipBoard object
   */
  @Override
  public int getWidth() {
    return width;
  }

  /**
   * Returns the filed height of BattleShipBoard object
   * 
   * @return int height of BattleShipBoard object
   */
  @Override
  public int getHeight() {
    return height;
  }

  /**
   * Returns the iterator of the ships.
   * 
   * @return
   */
  @Override
  public Iterable<Ship<T>> getShips() {
    return this.myShips;
  }

  /**
   * Returns a ship at a specific coordinate, otherwise null
   */
  @Override
  public Ship<T> getShipAt(Coordinate coordinate) {
    for (Ship<T> ship : getShips()) {
      if (ship.occupiesCoordinates(coordinate)) {
        return ship;
      }
    }
    return null;
  }

  /**
   * Returns the missInfo.
   * 
   * @return
   */
  public T getMissInfo() {
    return this.missInfo;
  }

  /**
   * Adds a ship to the board.
   * 
   * @param toAdd
   * @return message object indicates if adding is successful
   */
  @Override
  public String tryAddShip(Ship<T> toAdd) {
    String message = this.placementChecker.checkPlacement(toAdd, this);
    if (message != null) {
      return message;
    }
    myShips.add(toAdd);
    return null;
  }

  /**
   * Move the ship toMove to the afterMove location.
   */
  @Override
  public String tryMoveShip(Ship<T> toMove, Ship<T> afterMove) {
    // temporarily remove toMove
    this.myShips.remove(toMove);
    String message = tryAddShip(afterMove);
    if (message != null) {
      // add fails, restore original ship
      this.myShips.add(toMove);
      return message;
    }
    // add succesfully,copy the hitted parts
    copyHittedParts(toMove, afterMove);
    return null;
  }

  /**
     * Copy the hitted parts
     * @param toMove
     * @param afterMove
     */
    @Override
    public void copyHittedParts(Ship<T> toMove, Ship<T> afterMove) {
      for (Coordinate hitted : toMove.getHittedParts()) {
        int rowDiff = Math.abs(hitted.getRow() - toMove.getPivot().getRow());
        int colDiff = Math.abs(hitted.getColumn() - toMove.getPivot().getColumn());
        for (Coordinate c : afterMove.getCoordinates()) {
          // find the corresponding relative point
          if (Math.abs(c.getRow() - afterMove.getPivot().getRow()) == rowDiff
            && Math.abs(c.getColumn() - afterMove.getPivot().getColumn()) == colDiff) {
              afterMove.recordHitAt(c);
            }
        }
      }
    }

  /**
   * General what is at, handles both for self and for the enemy
   * 
   * @param where
   * @param isSelf
   * @return the info at the position
   */
  protected T whatIsAt(Coordinate where, boolean isSelf) {
    if (isSelf) {
      for (Ship<T> s : myShips) {
        if (s.occupiesCoordinates(where)) {
          return s.getDisplayInfoAt(where, isSelf);
        }
      }
    } else {
      // show for the enemy if he/she misses
      if (this.enemyMisses.contains(where)) {
        return this.missInfo;
      }
      if (this.enemyHits.containsKey(where)) {
        return this.enemyHits.get(where).getDisplayInfoAt(where, isSelf);
      }
    }
    return null;
  }

  /**
   * Gets the information for self's ship at a specific location.
   * 
   * @param where
   * @return T the specified generics
   */
  @Override
  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  /**
   * Gets the information for self's ship at a specific location.
   * 
   * @param where
   * @return T the specified generics
   */
  @Override
  public T whatIsAtForEnemy(Coordinate where) {
    return whatIsAt(where, false);
  }

  /**
   * Fire at the given coordinate c, return a ship if hiited, otherweise records
   * miss and returns null
   * 
   * @param c
   * @return
   */
  @Override
  public Ship<T> fireAt(Coordinate c) {
    for (Ship<T> ship : this.myShips) {
      if (ship.occupiesCoordinates(c)) {
        ship.recordHitAt(c);
        this.enemyHits.put(c, ship);
        if (this.enemyMisses.contains(c)) {
          this.enemyMisses.remove(c);
        }
        return ship;
      }
    }
    this.enemyMisses.add(c);
    if (this.enemyHits.containsKey(c)) {
      this.enemyHits.remove(c);
    }
    return null;
  }

  /**
   * Checks for completion given the completion rules.
   */
  @Override
  public boolean checkCompletion() {
    return this.completionRules.checkCompletion(this);
  }

  /**
   * Check if current ship's orientation satisfies the requirement
   * 
   * @return
   */
  @Override
  public boolean checkOrientation(String shipName, char orientation) {
    return this.orientationRuleChecker.checkOrientation(shipName, orientation);
  }
}
