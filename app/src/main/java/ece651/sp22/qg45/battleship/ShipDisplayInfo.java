package ece651.sp22.qg45.battleship;

/**
 * Interface provding getInfo method.
 */
public interface ShipDisplayInfo<T> {
    public T getInfo(Coordinate where, boolean hit);
}
