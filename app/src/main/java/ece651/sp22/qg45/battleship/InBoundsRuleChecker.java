package ece651.sp22.qg45.battleship;

/**
 * Responsible to check if the placing ship is out of the board.
 */
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * Constructs the InBoundChecker.
     * 
     * @param next
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Overriden the checker such that to check if in bound.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (0 <= c.getRow() && c.getRow() < theBoard.getHeight()
                    && 0 <= c.getColumn() && c.getColumn() < theBoard.getWidth()) {
                continue;
            } else if (c.getRow() < 0) {
                return "That placement is invalid: the ship goes off the top of the board.";
            } else if (c.getRow() >= theBoard.getHeight()) {
                return "That placement is invalid: the ship goes off the bottom of the board.";
            } else if (c.getColumn() < 0) {
                return "That placement is invalid: the ship goes off the left of the board.";
            } else {
                return "That placement is invalid: the ship goes off the right of the board.";
            }
        }
        return null;
    }

}
