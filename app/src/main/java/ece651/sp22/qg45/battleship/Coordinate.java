package ece651.sp22.qg45.battleship;

/**
 * Class that is used to store the position information.
 */
public class Coordinate {
    private final int row;
    private final int col;

    /**
     * Constructs a coordinate object, given the row and col value.
     * 
     * @param row
     * @param col
     */
    public Coordinate(int row, int col) {
        this.row = row;
        this.col = col;
    }
    
    /**
     * Constructs a coordinate object, given the string which contains position information.
     * @param descr the string which contains position information.
     * @throws IllegalArgumentException if input is invalid.
     */
    public Coordinate(String descr) {
        // check length
        if (descr == null || descr.length() != 2) {
            throw new IllegalArgumentException("Input postion information's length is not 2.");
        }

        char rowLetter = descr.charAt(0);
        char colLetter = descr.charAt(1);
        // check In range
        if (!(('A' <= rowLetter && rowLetter <= 'Z') || ('a' <= rowLetter && rowLetter <= 'z'))) {
            throw new IllegalArgumentException("RowLetter is out of range of A-Z");
        }
        rowLetter = Character.toUpperCase(rowLetter);
        
        if (colLetter < '0' || colLetter > '9') {
            throw new IllegalArgumentException("ColLetter is out of range of 0-9");
        }

        this.row = rowLetter - 'A';
        this.col = colLetter - '0';
    }
    /**
     * Returns the row field of a coordinate object
     * 
     * @return int the row field of a coordinate object
     */
    public int getRow() {
        return row;
    }

    /**
     * Returns the col field of a coordinate object
     * 
     * @return int the col field of a coordinate object
     */
    public int getColumn() {
        return col;
    }

    /**
     * Overriden equals() which checks equality of coordinate object
     * with other objects.
     * 
     * @return boolean equality result
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && col == c.col;
        }
        return false;
    }

    /**
     * Overriden toString()
     */
    @Override
    public String toString() {
        return "(" + row + ", " + col + ")";
    }

    /**
     * Overriden hashCode() in order to be compatible
     * with hasing data structures.
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
