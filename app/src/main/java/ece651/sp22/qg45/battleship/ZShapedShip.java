package ece651.sp22.qg45.battleship;

import java.util.HashSet;

/**
 * Z shaped ship class.
 */
public class ZShapedShip<T> extends BasicShip<T> {
    final String name;

    /**
     * Contructs T shaped ship.
     * 
     * @param name
     * @param upperLeft
     * @param orientation
     * @param myDisplayInfo
     * @param enemyDisplayInfo
     */
    public ZShapedShip(String name, Coordinate upperLeft, char orientation, ShipDisplayInfo<T> myDisplayInfo,
            ShipDisplayInfo<T> enemyDisplayInfo) {
        super(adjustPivot(upperLeft, orientation), makeCoords(upperLeft, orientation), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    public ZShapedShip(String name, Coordinate upperLeft, Character orientation, T data, T onHit) {
        this(name, upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * Adjusts pivot.
     * @param upperLeft
     * @param orientation
     * @return
     */
    static Coordinate adjustPivot(Coordinate upperLeft, char orientation) {
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        switch (orientation) {
            case 'U':
                break;
            case 'R':
                col += 4;
                break;
            case 'D':
                row += 4;
                col += 1;
                break;
            case 'L':
                row += 1;
                break;
        }
        return new Coordinate(row, col);
    }
    /**
     * Generates the set of coordinates that the ship occupies.
     * 
     * @param upperLeft
     * @param orientation
     * @return HashSet<Coordinate>
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
        HashSet<Coordinate> coordinates = new HashSet<>();
        int row = upperLeft.getRow();
        int col = upperLeft.getColumn();
        switch (orientation) {
            case 'U':
                coordinates.add(new Coordinate(row, col));
                coordinates.add(new Coordinate(row + 1, col));
                coordinates.add(new Coordinate(row + 2, col));
                coordinates.add(new Coordinate(row + 3, col));
                coordinates.add(new Coordinate(row + 2, col + 1));
                coordinates.add(new Coordinate(row + 3, col + 1));
                coordinates.add(new Coordinate(row + 4, col + 1));
                break;
            case 'R':
                coordinates.add(new Coordinate(row, col + 1));
                coordinates.add(new Coordinate(row, col + 2));
                coordinates.add(new Coordinate(row, col + 3));
                coordinates.add(new Coordinate(row, col + 4));
                coordinates.add(new Coordinate(row + 1, col));
                coordinates.add(new Coordinate(row + 1, col + 1));
                coordinates.add(new Coordinate(row + 1, col + 2));
                break;
            case 'D':
                coordinates.add(new Coordinate(row, col));
                coordinates.add(new Coordinate(row + 1, col));
                coordinates.add(new Coordinate(row + 2, col));
                coordinates.add(new Coordinate(row + 1, col + 1));
                coordinates.add(new Coordinate(row + 2, col + 1));
                coordinates.add(new Coordinate(row + 3, col + 1));
                coordinates.add(new Coordinate(row + 4, col + 1));
                break;
            case 'L':
                 coordinates.add(new Coordinate(row, col + 2));
                 coordinates.add(new Coordinate(row, col + 3));
                 coordinates.add(new Coordinate(row, col + 4));
                 coordinates.add(new Coordinate(row + 1, col));
                 coordinates.add(new Coordinate(row + 1, col + 1));
                 coordinates.add(new Coordinate(row + 1, col + 2));
                 coordinates.add(new Coordinate(row + 1, col + 3));
                break;
        }
        return coordinates;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
