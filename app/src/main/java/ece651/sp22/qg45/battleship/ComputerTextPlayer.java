package ece651.sp22.qg45.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * We only consider compter text player this time, so we just extends the text
 * player.
 */

public class ComputerTextPlayer extends TextPlayer {
  final PrintStream toTell;

  /**
   * Constructs the computer text player.
   * 
   * @param name
   * @param theBoard
   * @param inputReader
   * @param out
   * @param toTell
   * @param shipFactory
   * @param availableMoves
   * @param availableScans
   */
  public ComputerTextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
      PrintStream toTell, AbstractionShipFactory<Character> shipFactory, int availableMoves, int availableScans) {
    super(name, theBoard, inputReader, out, shipFactory, availableMoves, availableScans);
    this.toTell = toTell;
  }

  public ComputerTextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream toTell,
      AbstractionShipFactory<Character> shipFactory) {
    this(name, theBoard, inputReader, new PrintStream(OutputStream.nullOutputStream()), toTell, shipFactory, 0, 0);
  }

  /**
   * Overridden the behaviour, only do simple attack.
   */
  @Override
  public void doAttackingPhase(TextPlayer another) throws IOException {
    doOneAttack(another);
  }

  @Override
  public void doOneAttack(TextPlayer another) throws IOException{
      Coordinate coordinate = readCoordinate("");
      Ship<Character> hittedShip = another.getBoard().fireAt(coordinate);
      if (hittedShip != null) {
        String message = "Player " + this.name + " hit your " + hittedShip.getName() + " at " + coordinate;
        this.toTell.println(message);
      }
  }

  /**
   * Display the message on the other side.
   */
  @Override
  public void announceWin() {
    this.toTell.println("Player " + this.name + " win!\n");
  }
}
