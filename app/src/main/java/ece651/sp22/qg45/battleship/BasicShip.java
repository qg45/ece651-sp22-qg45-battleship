package ece651.sp22.qg45.battleship;

import java.util.ArrayList;

/**
 * Abstract class for ship.
 */

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
    protected Coordinate pivot; // pivot point for rotation
    protected HashMap<Coordinate, Boolean> myPieces; // null: not part of ship F: not hitted T: hitted
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    /**
     * Constructs the ship given a iterable list of coordinates.
     * 
     * @param where
     */
    public BasicShip(Coordinate upperLeft, Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo,
    ShipDisplayInfo<T> enemyDisplayInfo) {
        this.myPieces = new HashMap<>();
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        for (Coordinate c : where) {
            this.myPieces.put(c, false);
        }
        this.pivot = upperLeft;
    }

    /**
     * Tells if a specific postion is occupied by the ship
     * 
     * @returns boolean true if if a specific postion is occupied by the ship
     */
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        if (this.myPieces.get(where) == null) {
            return false;
        }
        return true;
    }

    /**
     * Tells if a coordinate is in the body of ship
     * 
     * @param c coordinate
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (!this.myPieces.containsKey(c)) {
            throw new IllegalArgumentException("Ship dose not occupy " + c);
        }
    }

    /**
     * Check if the ship is sunk.
     */
    @Override
    public boolean isSunk() {
        for (Coordinate c : this.myPieces.keySet()) {
            if (!wasHitAt(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Records the hit part.
     */
    @Override
    public void recordHitAt(Coordinate where) {
        this.myPieces.put(where, true);

    }

    /**
     * Tells if a position is hitted.
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        return this.myPieces.get(where);
    }

    /**
     * Returns the display info based on own or enymy's view.
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        if (myShip) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        return enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }

    /**
     * Overiden getCoordinates(). Returns key set of the myPieces.
     */
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return this.myPieces.keySet();
    }

    /**
     * Returns the pivot point
     * @return the piovt point to the ship
     */
    @Override
    public Coordinate getPivot() {
        return this.pivot;
    }

    /**
     * Returns the hitted parts of the ship.
     * @return
     */
    @Override
    public Iterable<Coordinate> getHittedParts() {
        ArrayList<Coordinate> hitted = new ArrayList<>();
        for (Coordinate c : getCoordinates()) {
            if (myPieces.get(c)) {
                hitted.add(c);
            }
        }
        return hitted;
    }
}
