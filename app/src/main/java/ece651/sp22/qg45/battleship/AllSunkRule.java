package ece651.sp22.qg45.battleship;

/**
 * One Completion Rule: all ships on board are sunk.
 */
public class AllSunkRule<T> extends CompletionRules<T> {

    /**
     * Constructors.
     * @param next
     */
    public AllSunkRule(CompletionRules<T> next) {
        super(next);
    }

    /**
     * Check if all ships on the board are sunk.
     */
    @Override
    protected boolean checkMyRule(Board<T> theBoard) {
        for (Ship<T> ship : theBoard.getShips()) {
            if (!ship.isSunk()) {
                return false;
            }
        }
        return true;
    }
    
}
