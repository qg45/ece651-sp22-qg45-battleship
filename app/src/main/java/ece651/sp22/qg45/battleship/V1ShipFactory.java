package ece651.sp22.qg45.battleship;

/**
 * Factory class creating version1 ships.
 */
public class V1ShipFactory implements AbstractionShipFactory<Character> {
    /**
     * Abtracted method to create version 1 ship.
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if (where.getOrientation() == 'H') { // if horizontal, reverse width and height
            return new RectangleShip<Character>(name, where.getCoordinate(), h, w, letter, '*');
        }
        return new RectangleShip<Character>(name, where.getCoordinate(), w, h, letter, '*');
    }

    /**
     * Make version1 submarine.
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    /**
     * Make version1 battleship.
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    /**
     * Make version1 carrier.
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    /**
     * Make version1 destroyer.
     */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

}
