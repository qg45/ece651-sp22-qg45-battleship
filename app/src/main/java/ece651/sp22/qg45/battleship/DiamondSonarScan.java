package ece651.sp22.qg45.battleship;

/**
 * Diamond sonar scan that implements sonarscan interface.
 */
import java.util.HashMap;

public class DiamondSonarScan<T> implements SonarScan<T>{
    final Coordinate center;
    final Board<T> toScan;
    final HashMap<String, Integer> shipToSquares;
    final int scanLength;

    /**
     * Constructs the scanner based on a given board.
     * @param toScan
     */
    public DiamondSonarScan(Coordinate center, Board<T> toScan, int scanLength) {
        this.center = center;
        this.toScan = toScan;
        this.shipToSquares = new HashMap<>();
        this.scanLength = scanLength;
        setUpShipToNumber();
    }

    /**
     * Default length is 3 as in requirement.
     * @param center
     * @param toScan
     */
    public DiamondSonarScan(Coordinate center, Board<T> toScan) {
        this(center, toScan, 3);
    }
    
    /**
     * Scans a board and records the result.
     */
    @Override
    public void doScan() {
        int row = this.center.getRow();
        int col = this.center.getColumn();
        for (int i = row - this.scanLength; i <= row + this.scanLength; i++) {
            int colAdjust = this.scanLength - Math.abs(row - i);
            for (int j = col - colAdjust; j <= col + colAdjust; j++) {
                if (0 <= i && i < this.toScan.getHeight()
                     && 0 <= j && j < this.toScan.getWidth()) {
                         Ship<T> ship = this.toScan.getShipAt(new Coordinate(i, j));
                         if (ship != null) {
                             int count = this.shipToSquares.get(ship.getName()) + 1;
                             this.shipToSquares.put(ship.getName(), count);
                         }
                     }
            }
        }
    }

    /**
     * Reports the scan result.
     * 
     * @return String the ship : number in the scan region
     */
    @Override
    public String reportResult() {
        StringBuilder result = new StringBuilder();
        for (String shipName : this.shipToSquares.keySet()) {
            result.append(shipName);
            result.append(" occupy " + this.shipToSquares.get(shipName) + " squares\n");
        }
        return result.toString();
    }
    
    /**
     * Initialize all counts as 0
     */
    private void setUpShipToNumber() {
        for (Ship<T> ship : this.toScan.getShips()) {
            this.shipToSquares.put(ship.getName(), 0);
        }
    }
}
