package ece651.sp22.qg45.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board (i.e., converting it to a
 * string to show to the user). It supports two ways to display the Board: one
 * for the player's own board, and one for the enemy's board.
 */
public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;

  /**
   * Constructs a BoardView, given the board it will display.
   * 
   * @param toDisplay is the Board to display
   * @throws IllegalArgumentException if the board is larger than 10x26.
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
  }

  /**
   * Shows two boards.
   * 
   * @param enemyView
   * @param myHeader
   * @param enemyHeader
   * @return String the two boards
   */
  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    StringBuilder boards = new StringBuilder();
    String[] selfBoard = this.displayMyOwnBoard().split("\n");
    String[] enemyBoard = enemyView.displayEnemyBoard().split("\n");
    // append header
    boards.append(" ".repeat(5) + myHeader);
    int w = enemyView.toDisplay.getWidth();
    int spaceRepeat = 2 * w + 22 - boards.length();
    boards.append(" ".repeat(spaceRepeat)).append(enemyHeader + "\n");

    // append board
    StringBuilder row = new StringBuilder();
    for (int i = 0; i < selfBoard.length; i++) {
      row.append(selfBoard[i]);
      row.append(" ".repeat(16));
      if (i == 0 || i == selfBoard.length - 1) { // first and last row
        row.append("  ");
      }
      row.append(enemyBoard[i] + "\n");
      boards.append(row.toString());
      row.setLength(0);
    }

    return boards.toString();
  }

  /**
   * This returns string which represents the self board.
   * 
   * @return String the string which represents the empty board.
   */
  public String displayMyOwnBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
  }

  /**
   * This returns string which represents the enemy board.
   * 
   * @return String the string which represents the empty board.
   */

  public String displayEnemyBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
  }

  /**
   * Helper function for making any board
   */
  private String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
    StringBuilder board = new StringBuilder();
    board.append(makeHeader());
    board.append(makeRows(getSquareFn));
    board.append(makeHeader());
    return board.toString();
  }

  /**
   * This makes the header line, e.g. 0|1|2|3|4\n
   * 
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder header = new StringBuilder("  "); // README shows two spaces at
    String sep = ""; // start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      header.append(sep);
      header.append(i);
      sep = "|";
    }
    header.append("\n");
    return header.toString();
  }

  /**
   * This makes the row lines in the middle, e.g. A | ... | A\n, B | ... | B\n,
   * according to the hegiht of the board
   * 
   * @return String that is the row lines for the given board
   */
  String makeRows(Function<Coordinate, Character> getSquareFn) {
    StringBuilder rows = new StringBuilder();
    char rowIdx = 'A';
    for (int i = 0; i < toDisplay.getHeight(); i++) {
      rows.append(makeRow(rowIdx, getSquareFn));
      ++rowIdx;
    }
    return rows.toString();
  }

  /**
   * Helper function which returns the specific row given the row character
   * 
   * @return String the sepcific row line
   */
  String makeRow(char rowIdx, Function<Coordinate, Character> getSquareFn) {
    String sep = "|";
    StringBuilder row = new StringBuilder();
    row.append(rowIdx);
    row.append(" ");
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      Character c = getSquareFn.apply(new Coordinate(Character.toString(rowIdx) + i));
      if (c == null) {
        row.append(" ");
      } else {
        row.append(c);
      }
      row.append(sep);
    }
    row.deleteCharAt(row.length() - 1); // remove the extra sep
    row.append(" ");
    row.append(rowIdx);
    row.append("\n");
    return row.toString();
  }
}
