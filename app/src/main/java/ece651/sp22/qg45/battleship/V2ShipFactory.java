package ece651.sp22.qg45.battleship;

/**
 * Version 2 factory.
 */
public class V2ShipFactory extends V1ShipFactory{
    /**
     * T-shaped battleship.
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return new TShapedShip<Character>("Battleship", where.getCoordinate(), where.getOrientation(), 'b', '*');
    }

    /**
     * Z-shaped carrier.
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return new ZShapedShip<Character>("Carrier", where.getCoordinate(), where.getOrientation(), 'c', '*');
    }
}
