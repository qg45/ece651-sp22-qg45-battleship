package ece651.sp22.qg45.battleship;

/**
 * Abstract class for general chaining rule check.
 */
public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;

    /**
     * Constructors. HAS_A relationship
     * @param next
     */
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * Rule need to be overriden by specific checker
     * @param theShip
     * @param theBoard
     * @return
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * Public method for chaining check.
     */
    public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String message = checkMyRule(theShip, theBoard);
        if (message != null) {
          return message;
        }
        //other wise, ask the rest of the chain.
        if (next != null) {
          return next.checkPlacement(theShip, theBoard); 
        }
        //if there are no more rules, then the placement is legal
        return null;
      }
}
