package ece651.sp22.qg45.battleship;

/**
 * The main class that actually runs the battleship game.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;

public class App {
    final TextPlayer player1;
    final TextPlayer player2;

    /**
     * main function for run
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        PrintStream out = System.out;
        // V1ShipFactory factory = new V1ShipFactory();
        V2ShipFactory factory = new V2ShipFactory();
        int mode = getMode(input, out);
        TextPlayer p1 = null;
        TextPlayer p2 = null;
        switch (mode) {
            case 1:
                p1 = new TextPlayer("A", b1, input, out, factory);
                p2 = new TextPlayer("B", b2, input, out, factory);
                break;
            case 2:
                p1 = new TextPlayer("A", b1, input, out, factory);
                p2 = new ComputerTextPlayer("B", b2, generateComputerInput(b2), out, factory);
                break;
            case 3:
                p1 = new ComputerTextPlayer("A", b1, generateComputerInput(b1), out, factory);
                p2 = new TextPlayer("B", b2, input, out, factory);
                break;
            case 4:
                p1 = new ComputerTextPlayer("A", b1, generateComputerInput(b1), out, factory);
                p2 = new ComputerTextPlayer("B", b2, generateComputerInput(b2), out, factory);
                break;
        }
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }

    /**
     * Initialize app with two players.
     */
    public App(TextPlayer player1, TextPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    /**
     * Inlucde two players to do the placement phase.
     * 
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        this.player1.doPlacementPhase();
        this.player2.doPlacementPhase();
    }

    /**
     * Attacking phase.
     */
    public void doAttackingPhase() throws IOException {
        while (true) {
            this.player1.doAttackingPhase(this.player2);

            if (this.player2.isLose()) {
                this.player1.announceWin();
                return;
            }

            this.player2.doAttackingPhase(this.player1);

            if (this.player1.isLose()) {
                this.player2.announceWin();
                return;
            }
        }
    }

    /**
     * Generate computer input based on the board
     * 
     * @param theBoard
     * @return
     */
    private static BufferedReader generateComputerInput(Board<Character> theBoard) {
        // placement
        StringBuilder input = new StringBuilder("a0v\na1v\na2v\na3v\na4v\na5d\na7l\nc0r\nf0u\nq0r\n");
        // attacking
        for (int i = 0; i < theBoard.getHeight(); i++) {
            for (int j = 0; j < theBoard.getWidth(); j++) {
                input.append((char) ('a' + i) + Integer.toString(j) + "\n");
            }
        }
        return new BufferedReader(new StringReader(input.toString()));
    }

    /**
     * Choose the mode based on user input
     * 
     * @param input
     * @param out
     * @return
     * @throws IOException
     */
    private static int getMode(BufferedReader input, PrintStream out) throws IOException {
        String prompt = "Choose the mode you like: \n" + "1. Human vs Human\n" + "2. Human vs Computer\n"
                + "3. Computer vs Human\n"
                + "4. Computer vs Computer\n";
        out.println(prompt);
        String s = input.readLine();
        if (!s.equals("1") && !s.equals("2") && !s.equals("3") && !s.equals("4")) {
            out.println("Your input: " + s + " is not valid");
            return getMode(input, out);
        }
        return Integer.parseInt(s);
    }
}
