package ece651.sp22.qg45.battleship;

public abstract class OrientationRuleChecker {
    private final OrientationRuleChecker next;

  /**
   * Constructors. HAS_A relationship
   * 
   * @param next
   */
  public OrientationRuleChecker(OrientationRuleChecker next) {
    this.next = next;
  }

  /**
   * Rule need to be overriden by specific checker
   * 
   * @param theShip
   * @return
   */
  protected abstract boolean checkMyRule(String name, char orientation);

  /**
   * Public method for chaining check.
   */
  public boolean checkOrientation(String name, char orientation) {
    // if we fail our own rule: stop the placement is not legal

    if (checkMyRule(name, orientation)) {
      return true;
    }
    // other wise, ask the rest of the chain.
    if (next != null) {
      return next.checkOrientation(name, orientation);
    }
    // if there are no more rules, then the placement is legal
    return false;
  }
}
