package ece651.sp22.qg45.battleship;

/**
 * Interface for scanner, define the do Scan method.
 */
public interface SonarScan<T> {
    /**
     * Scans a board and records the result.
     */
    public void doScan();

    /**
     * Reports the scan result.
     * 
     * @return String the ship : number in the scan region
     */
    public String reportResult();
}
