package ece651.sp22.qg45.battleship;

/**
 * SimpleShipDisplayInfo which displays the info at certain coordination given
 * hit status.
 */
public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    T myData;
    T onHit;

    /**
     * Constructs given myData and onHit.
     * 
     * @param myData
     * @param onHit
     */
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    /**
     * Returns specific data given hit status.
     */
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return this.onHit;
        }
        return this.myData;
    }

}
