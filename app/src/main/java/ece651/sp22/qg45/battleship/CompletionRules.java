package ece651.sp22.qg45.battleship;

/**
 * Abstract class for chaining game completion check.
 */
public abstract class CompletionRules<T> {
  private final CompletionRules<T> next;

  /**
   * Constructors. HAS_A relationship
   * 
   * @param next
   */
  public CompletionRules(CompletionRules<T> next) {
    this.next = next;
  }

  /**
   * Rule need to be overriden by specific checker
   * 
   * @param theShip
   * @return
   */
  protected abstract boolean checkMyRule(Board<T> theBoard);

  /**
   * Public method for chaining check.
   */
  public boolean checkCompletion(Board<T> theBoard) {
    // if we fail our own rule: stop the placement is not legal

    if (checkMyRule(theBoard)) {
      return true;
    }
    // other wise, ask the rest of the chain.
    if (next != null) {
      return next.checkCompletion(theBoard);
    }
    // if there are no more rules, then the placement is legal
    return false;
  }
}
