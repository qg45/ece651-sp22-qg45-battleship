package ece651.sp22.qg45.battleship;

/**
 * Board interface provides general methods for the classes
 * that equip the board property.
 */
public interface Board<T> {
    /**
     * Returns the filed width of Board object
     * 
     * @return int width of Board object
     */
    public int getWidth();

    /**
     * Returns the filed height of Board object
     * 
     * @return int height of Board object
     */
    public int getHeight();

    /**
     * Returns an iterator of the ship container.
     * @return
     */
    public Iterable<Ship<T>> getShips();

    /**
     * Returns a ship at a specifc coordinate
     * @return Ship<T>
     */

    public Ship<T> getShipAt(Coordinate coordinate);

    /**
     * Gets the missInfo of the board.
     * @return
     */
    public T getMissInfo();
    
    /**
     * Adds ship to board.
     * @param toAdd
     * @return boolean
     */
    public String tryAddShip(Ship<T> toAdd);

    /**
     * Gets the information on the specific coordinate.
     * @param where
     * @return
     */
    public T whatIsAtForSelf(Coordinate where);


    /**
     * Gets the information on the specific coordinate.
     * @param where
     * @return
     */
    public T whatIsAtForEnemy(Coordinate where);

    /**
     * Enable borad to have a shoot on the ship.
     * @param c
     * @return specific object for further opertion
     */
    public Ship<T> fireAt(Coordinate c);

    /**
     * Check if current board satisfies the completion rule.
     * @return boolean true if complete
     */
    public boolean checkCompletion();

    /**
     * Check if current ship's orientation satisfies the requirement
     * @return boolean
     */
    public boolean checkOrientation(String shipName, char orientation);

    /**
     * Moves a ship to a new coordinate with a new orientation.
     * @param toMove
     * @param afterMove
     * @return
     */
    public String tryMoveShip(Ship<T> toMove, Ship<T> afterMove);

    /**
     * Copy the hitted parts
     * @param toMove
     * @param afterMove
     */
    public void copyHittedParts(Ship<T> toMove, Ship<T> afterMove);
}
