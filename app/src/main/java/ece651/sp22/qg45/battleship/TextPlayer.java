package ece651.sp22.qg45.battleship;

/**
 * Class for text player.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    final String name;
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractionShipFactory<Character> shipFactory;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    final ArrayList<String> shipToPlace;
    private int availableMoves;
    private int availableScans;

    /**
     * Constructs the text player object.
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
            AbstractionShipFactory<Character> shipFactory, int availableMoves, int availableScans) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipCreationFns = new HashMap<>();
        this.shipToPlace = new ArrayList<>();
        this.availableMoves = availableMoves;
        this.availableScans = availableScans;
        setupShipCreationList();
        setupShipCreationMap();
    }

    /**
     * Constructs the text player object. Default for 3 moves chance.
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
            AbstractionShipFactory<Character> shipFactory) {
        this(name, theBoard, inputReader, out, shipFactory, 3, 3);
    }

    /**
     * Prompts the message to notify user to put and read in the user input.
     * 
     * @param prompt
     * @return
     * @throws IOException
     */
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        Placement placement = null;
        try {
            placement = new Placement(s);
        } catch (IllegalArgumentException e) {
            out.println(e.getMessage());
            return readPlacement(prompt);
        }
        return placement;
    }

    

    /**
     * Prompts the message to notify user to put and read in the user input of
     * coordinate.
     * 
     * @param prompt
     * @return
     * @throws IOException
     */
    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        Coordinate coordinate = null;
        try {
            coordinate = new Coordinate(s);
        } catch (IllegalArgumentException e) {
            out.println(e);
            return readCoordinate(prompt);
        }
        return coordinate;
    }

    /**
     * Read the player's action. (version 2)
     * @param prompt
     * @return
     * @throws IOException
     */
    public String readAction(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return s.toUpperCase();
    }

    /**
     * Place a ship given its name and creation function
     * 
     * @param shipName
     * @param createFn
     * @throws IOException
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        String prompt = "Player " + this.name + " where do you want to place a " + shipName + "?";
        Placement p = readPlacement(prompt);
        if (!this.theBoard.checkOrientation(shipName, p.getOrientation())) {
            String message = p.getOrientation() + " is not suitable for " + shipName;
            this.out.println(message);
            doOnePlacement(shipName, createFn);
            return;
        }
        Ship<Character> ship = createFn.apply(p);
        String message = this.theBoard.tryAddShip(ship);
        if (message != null) {
            this.out.println(message);
            doOnePlacement(shipName, createFn);
            return;
        }
        this.out.println(this.view.displayMyOwnBoard());
    }

    /**
     * Do placement phase.
     * 
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        this.out.println(view.displayMyOwnBoard());
        String instruction = "Player " + this.name + ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are T-Shaped\n" +
                "2 \"Carriers\" that are Z-Shaped\n";
        this.out.println(instruction);
        // doOnePlacement();
        // Iterate on the ship list to place ships
        for (String shipName : this.shipToPlace) {
            doOnePlacement(shipName, this.shipCreationFns.get(shipName));
        }
    }

    /**
     * Current player in the attacking phase to attack another one.
     * 
     * @param another
     */
    public void doAttackingPhase(TextPlayer another) throws IOException {
        String myHeader = "Your ocean";
        String enemyHeader = "Player " + another.name + "'s ocean";
        this.out.println(this.view.displayMyBoardWithEnemyNextToIt(another.view, myHeader, enemyHeader));
        String prompt = "Possible actions for Player " + this.name + ":\n\n" +
        " F Fire at a square\n" +
        " M Move a ship to another square (" + this.availableMoves + " remaining)\n" +
        " S Sonar scan (" + this.availableScans + " remaining)\n\n" +
        "Player " + this.name + ", what would you like to do?\n";
        String action = this.readAction(prompt);
        if (action.equals("F")) {
            doOneAttack(another);
        } else if (action.equals("M")) {
            if (this.availableMoves == 0) {
                this.out.println("You have no remaining moves!");
                doAttackingPhase(another);
            } else {
                doOneMoveShip(another);
            }
        } else if (action.equals("S")) {
            if (this.availableScans == 0) {
                this.out.println("You have no remaining scan!");
                doAttackingPhase(another);
            } else {
                doOneScan(another);
            }
        } else { // invalid input
            this.out.println("Your input action: "
             + action + " is not supported, pleae try again!");
            doAttackingPhase(another);
        }
    }

    /**
     * Do one attack on the another player.
     * 
     * @param another
     */
    public void doOneAttack(TextPlayer another) throws IOException {
        Coordinate coordinate = readCoordinate("Player " + this.name + ": Let's choose a coordinate to attack!");
        // repeated coordinate
        Character charOnBoard = another.theBoard.whatIsAtForEnemy(coordinate);
        if (charOnBoard != null) {
            // repeated miss
            if (charOnBoard.equals(another.theBoard.getMissInfo())) {
                this.out.println("This is already a missed point!\n");
                doOneAttack(another);
            // repeated attack
            } else {
                this.out.println("The square you have fired.\n");
                doOneAttack(another);
            }
            return;
        }
        Ship<Character> hittedShip = another.theBoard.fireAt(coordinate);
        if (hittedShip == null) {
            this.out.println("You miss!\n");
        } else {
            this.out.println("You hit a " + hittedShip.getName() + "!\n");
        }
    }

    /**
     * Player moves one ship
     * @param another
     * @throws IOException
     */
    public void doOneMoveShip(TextPlayer another) throws IOException {
        Coordinate from = readCoordinate("Player " + this.name + ": Let's choose a coordinate of ship that you want to move!");
        Ship<Character> ship = this.theBoard.getShipAt(from);
        if (ship == null) {
            this.out.println("This location does not exist a ship!");
            doAttackingPhase(another);
            return;
        }
        Placement to = readPlacement("Player " + this.name + ": Let's choose a coordinate with orientation that you want your ship to go!"); 
        if (!this.theBoard.checkOrientation(ship.getName(), to.getOrientation())) {
            this.out.println("The orientation " + to.getOrientation() + " is not compatible with " + ship.getName());
            doAttackingPhase(another);
            return;
        }
        // create a potential ship at the placement to
        Ship<Character> shipAfterMove = this.shipCreationFns.get(ship.getName()).apply(to);
        String message = this.theBoard.tryMoveShip(ship, shipAfterMove);
        if (message != null) {
            this.out.println(message);
            doAttackingPhase(another);
            return;
        }
        this.out.println("Move: " + ship.getName() + " successfully.");
        --this.availableMoves;
    }

    public void doOneScan(TextPlayer another) throws IOException{
        String prompt = "Player " + this.name + ": Let's choose an center coordinate for your sonar scan.";
        Coordinate center = readCoordinate(prompt);
        SonarScan<Character> scan = new DiamondSonarScan<>(center, another.theBoard);
        scan.doScan();
        this.out.println(scan.reportResult());
        --this.availableScans;
    }

    /**
     * Maps the ship name to its create function
     */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    }

    /**
     * Sets up the ships to place
     */
    protected void setupShipCreationList() {
        shipToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /**
     * Check if current palyer is lose
     * @return
     */
    public boolean isLose() {
        return theBoard.checkCompletion();
    }

    /**
     * Annouce current win.
     * @return
     */
    public void announceWin() {
        this.out.println("Player " + this.name + " win!\n");
    }

    /**
     * Returns the board of the player.
     * @return
     */
    public Board<Character> getBoard() {
        return this.theBoard;
    }
}
