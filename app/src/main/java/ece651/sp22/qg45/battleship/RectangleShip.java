package ece651.sp22.qg45.battleship;

/**
 * Rectangle Ship class for version 1.
 */
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    final String name;

    /**
     * Constructors creates the object of ship with name.
     * 
     * @param name
     * @param upperLeft
     * @param width
     * @param height
     * @param myDisplayInfo
     * @param enemyDisplayInfo
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,
            ShipDisplayInfo<T> enemyDisplayInfo) {
        super(upperLeft, makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * Constructor for not messing up the test
     * 
     * @param upperLeft
     * @param data
     * @param onHit
     */
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    /**
     * Generates the set of coordinates that the ship occupies.
     * 
     * @param upperLeft the upper-left coordinate of the ship
     * @param width     width of the ship
     * @param height    width of the ship
     * @return HashSet<Coordinate>
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> coordinates = new HashSet<>();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                coordinates.add(new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j));
            }
        }
        return coordinates;
    }

    /**
     * Retutns the name of ship
     */
    @Override
    public String getName() {
        return this.name;
    }
}
