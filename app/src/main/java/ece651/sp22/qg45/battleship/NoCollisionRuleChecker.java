package ece651.sp22.qg45.battleship;

/**
 * NoCollision Checker to check if the ship to place collides with the ships
 * have been put.
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

    /**
     * Constructs NoCollisionChecker instance.
     * 
     * @param next
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Overriden checker to check the collision.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(c) != null) { // if board already has stuff on specific posision
                return "That placement is invalid: the ship overlaps another ship.";
            }
        }
        return null;
    }

}
