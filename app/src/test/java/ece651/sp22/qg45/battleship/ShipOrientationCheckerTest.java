package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ShipOrientationCheckerTest {
  @Test
  public void test_checker() {
    OrientationRuleChecker checker = new ShipOrientationChecker(null);
    OrientationRuleChecker checker_ = new ShipOrientationChecker(checker);
    assertFalse(checker.checkMyRule("Submarine", 'R'));
    assertFalse(checker.checkOrientation("Submarine", 'R'));
    assertFalse(checker_.checkMyRule("Submarine", 'R'));
    assertFalse(checker_.checkOrientation("Submarine", 'R'));
    assertTrue(checker.checkMyRule("Submarine", 'V'));
    assertTrue(checker_.checkMyRule("Submarine", 'V'));
    assertFalse(checker.checkMyRule("Battleship", 'H'));
    assertFalse(checker_.checkMyRule("Battleship", 'H'));
    assertTrue(checker.checkMyRule("Battleship", 'R'));
  }

}
