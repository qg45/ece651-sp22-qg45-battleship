package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_in_b() {
    V1ShipFactory f = new V1ShipFactory();
    PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<>(null);
    Board<Character> b = new BattleShipBoard<>(3, 3,'X');
    Ship<Character> s1 = f.makeCarrier(new Placement(new Coordinate(0, 0), 'v'));
    Ship<Character> s2 = f.makeCarrier(new Placement(new Coordinate(0, 0), 'H'));
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.", checker.checkPlacement(s1, b));
    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkPlacement(s2, b));
    Ship<Character> s3 = f.makeSubmarine(new Placement(new Coordinate(0, 1), 'H'));
    Ship<Character> s4 = f.makeSubmarine(new Placement(new Coordinate(3, 0), 'H'));
    Ship<Character> s5 = f.makeSubmarine(new Placement(new Coordinate(0, 3), 'V'));
    Ship<Character> s5_ = f.makeSubmarine(new Placement(new Coordinate(0, 4), 'V'));
    Ship<Character> s5__ = f.makeSubmarine(new Placement(new Coordinate(0, 2), 'V'));
    assertNull(checker.checkPlacement(s3, b));
    assertNull(checker.checkPlacement(s5__, b));
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.", checker.checkPlacement(s4, b));
    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkPlacement(s5_, b));
    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkPlacement(s5, b));
    Ship<Character> s6 = f.makeSubmarine(new Placement(new Coordinate(-1, 0), 'v'));
    Ship<Character> s7 = f.makeSubmarine(new Placement(new Coordinate(0, -1), 'h'));
    assertEquals("That placement is invalid: the ship goes off the top of the board.", checker.checkPlacement(s6, b));
    assertEquals("That placement is invalid: the ship goes off the left of the board.", checker.checkPlacement(s7, b));
  }

}
