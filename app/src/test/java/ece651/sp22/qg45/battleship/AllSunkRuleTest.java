package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AllSunkRuleTest {
  @Test
  public void test_all_s_r() {
    AllSunkRule<Character> allSunkRule = new AllSunkRule<>(null);
    AllSunkRule<Character> allSunkRule_ = new AllSunkRule<>(new AllSunkRule<>(null));
    V1ShipFactory f = new V1ShipFactory();
    Board<Character> b = new BattleShipBoard<Character>(3, 3, 'X');
    b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(0, 0), 'h')));
    b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(1, 0), 'h')));
    assertFalse(allSunkRule.checkMyRule(b));
    assertFalse(allSunkRule.checkCompletion(b));
    assertFalse(allSunkRule_.checkMyRule(b));
    assertFalse(allSunkRule_.checkCompletion(b));
    b.fireAt(new Coordinate(0,1));
    assertFalse(allSunkRule.checkMyRule(b));
    assertFalse(allSunkRule.checkCompletion(b));
    b.fireAt(new Coordinate(1,0));
    assertFalse(allSunkRule.checkMyRule(b));
    b.fireAt(new Coordinate(0,0));
    b.fireAt(new Coordinate(1,1));
    assertTrue(allSunkRule.checkMyRule(b));
    assertTrue(allSunkRule.checkCompletion(b));
  }

}
