package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {
  @Test
  public void test_bs() {
    V2ShipFactory f = new V2ShipFactory();
    Ship<Character> s = f.makeBattleship(new Placement("a0r"));
    assertEquals(s.getName(), "Battleship");
  }

  @Test
  public void test_c() {
    V2ShipFactory f = new V2ShipFactory();
    Ship<Character> s = f.makeCarrier(new Placement("g0r"));
    assertEquals(s.getName(), "Carrier");
  }
}
