package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ZShapedShipTest {
  @Test
  public void test_construct() {
    Ship<Character> ship = new ZShapedShip<Character>("Carrier", new Coordinate(0, 0), 'R', 'c', '*');
    Ship<Character> ship1 = new ZShapedShip<Character>("Carrier", new Coordinate(0, 0), 'U', 'c', '*');
    Ship<Character> ship2 = new ZShapedShip<Character>("Carrier", new Coordinate(0, 0), 'L', 'c', '*');
    Ship<Character> ship3 = new ZShapedShip<Character>("Carrier", new Coordinate(0, 0), 'D', 'c', '*');
    assertEquals("Carrier", ship.getName());
    assertEquals("Carrier", ship1.getName());
    assertEquals("Carrier", ship2.getName());
    assertEquals("Carrier", ship3.getName());
  }

}
