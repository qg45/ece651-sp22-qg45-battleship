package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_miss_inof() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals('X', b1.getMissInfo());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  @Test
  public void test_board_with_ship() {
    V1ShipFactory f = new V1ShipFactory();
    BattleShipBoard<Character> b = new BattleShipBoard<>(3, 3, 'X');
    Character[][] expected = new Character[3][3];
    checkWhatIsAtBoard(b, expected);
    String ret = b.tryAddShip(new RectangleShip<Character>(new Coordinate(1, 2), 's', '*'));
    String ret_ = b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(0, 1), 'h')));
    assertEquals("That placement is invalid: the ship overlaps another ship.",
        b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(1, 1), 'h'))));
    assertEquals("That placement is invalid: the ship goes off the right of the board.",
        b.tryAddShip(f.makeCarrier(new Placement(new Coordinate(2, 0), 'h'))));
    assertNull(ret);
    assertNull(ret_);
    expected[1][2] = 's';
    expected[0][1] = expected[0][2] = 's';
    checkWhatIsAtBoard(b, expected);
  }

  @Test
  public void test_fire_at() {
    V1ShipFactory f = new V1ShipFactory();
    BattleShipBoard<Character> b = new BattleShipBoard<>(3, 3, 'X');
    Ship<Character> s = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'h'));
    b.tryAddShip(s);
    Coordinate c = new Coordinate(1, 0);
    b.enemyHits.put(c, s);
    Ship<Character> s_n = b.fireAt(c);
    assertNull(s_n);
    assertTrue(b.enemyMisses.contains(c));
    assertEquals('X', b.whatIsAtForEnemy(c));
    Coordinate c_ = new Coordinate(0, 1);
    b.enemyMisses.add(c_);
    Ship<Character> s_h = b.fireAt(c_);
    assertSame(s, s_h);
    assertFalse(b.enemyMisses.contains(c_));
    b.fireAt(new Coordinate(0,0));
    assertTrue(s.isSunk());
  }

  @Test
  public void test_two_dis() {
    V1ShipFactory f = new V1ShipFactory();
    Board<Character> b = new BattleShipBoard<Character>(4, 2, 'X');
    Board<Character> b_ = new BattleShipBoard<Character>(4, 2, 'X');
    BoardTextView view = new BoardTextView(b);
    BoardTextView view_ = new BoardTextView(b_);
    b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(0, 0), 'h')));
    b.tryAddShip(f.makeDestroyer(new Placement(new Coordinate(1, 1), 'h')));
    String header = " ".repeat(5) + " ".repeat(2 * 4 + 22 - 5) + "\n";
    String expected = header + "  0|1|2|3                    0|1|2|3\n" 
     + "A s|s| |  A                A  | | |  A\n" 
     + "B  |d|d|d B                B  | | |  B\n"
     + "  0|1|2|3                    0|1|2|3\n";
    assertEquals(expected, view.displayMyBoardWithEnemyNextToIt(view_, "", ""));
  }

  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
    for (int i = 0; i < expected.length; i++) {
      for (int j = 0; j < expected[0].length; j++) {
        assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
      }
    }
  }

  @Test
  public void test_get_ships() {
    V1ShipFactory f = new V1ShipFactory();
    Board<Character> b = new BattleShipBoard<Character>(3, 3, 'X');
    Ship<Character> ship = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'h'));
    b.tryAddShip(ship);
    b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(1, 0), 'h')));
    int count = 0;
    for (Ship<Character> s : b.getShips()) {
      ++count;
    }
    assertEquals(2, count);
    assertSame(ship, b.getShipAt(new Coordinate(0, 0)));
    assertNull(b.getShipAt(new Coordinate(2, 0)));
  }

  @Test
  public void test_compl() {
    V1ShipFactory f = new V1ShipFactory();
    Board<Character> b = new BattleShipBoard<Character>(3, 3, 'X');
    b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(0, 0), 'h')));
    b.tryAddShip(f.makeSubmarine(new Placement(new Coordinate(1, 0), 'h')));
    assertFalse(b.checkCompletion());
    b.fireAt(new Coordinate(0, 0));
    assertFalse(b.checkCompletion());
    b.fireAt(new Coordinate(1, 0));
    b.fireAt(new Coordinate(0, 1));
    b.fireAt(new Coordinate(1, 1));
    assertTrue(b.checkCompletion());
  }

  @Test
  public void test_try_move() {
    V2ShipFactory f = new V2ShipFactory();
    Board<Character> b = new BattleShipBoard<Character>(10, 10, 'X');
    BoardTextView view = new BoardTextView(b);
    Ship<Character> s1 = f.makeBattleship(new Placement("A0u"));
    Ship<Character> s2 = f.makeSubmarine(new Placement("c0h"));
    b.tryAddShip(s1);
    b.tryAddShip(s2);
    // System.out.println(view.displayMyOwnBoard());
    Ship<Character> s1_ = f.makeBattleship(new Placement("e0l"));
    assertNotNull(b.tryMoveShip(s1, s2));
    s1.recordHitAt(new Coordinate(1, 0));
    // System.out.println(s1.getPivot());
    int rowDiff = 0, colDiff = 0;
    assertNull(b.tryMoveShip(s1, s1_));
    for (Ship<Character> s : b.getShips()) {
      assertNotEquals(s, s1);
    }
    for (Coordinate c : s1_.getHittedParts()) {
      assertEquals(rowDiff, Math.abs(c.getRow() - s1_.getPivot().getRow()));
      assertEquals(colDiff, Math.abs(c.getColumn() - s1_.getPivot().getColumn()));
      // System.out.println(c);
    }
    // System.out.println(view.displayMyOwnBoard());
  }
}
