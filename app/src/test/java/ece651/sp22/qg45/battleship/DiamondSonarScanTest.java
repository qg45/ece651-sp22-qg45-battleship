package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DiamondSonarScanTest {
  @Test
  public void test_constructor() {
    V2ShipFactory f = new V2ShipFactory();
    Board<Character> b = new BattleShipBoard<Character>(10, 10, 'X');
    SonarScan<Character> scan = new DiamondSonarScan<>(new Coordinate(4, 1), b);
    scan.doScan();
    assertEquals(0, scan.reportResult().length());
    b.tryAddShip(f.makeBattleship(new Placement("a0u")));
    SonarScan<Character> scan_ = new DiamondSonarScan<>(new Coordinate(0, 0), b);
    scan_.doScan();
    scan_.reportResult();
    // System.out.println(scan_.reportResult());
  }

}
