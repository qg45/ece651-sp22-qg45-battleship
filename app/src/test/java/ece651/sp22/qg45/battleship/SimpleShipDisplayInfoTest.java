package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_constructor() {
    SimpleShipDisplayInfo<Character> shipDisplayInfo = new SimpleShipDisplayInfo<Character>('a', 'A');
    assertEquals('a', shipDisplayInfo.myData);
    assertEquals('A', shipDisplayInfo.onHit);
  }

  @Test
  public void test_get_info() {
    SimpleShipDisplayInfo<Character> shipDisplayInfo = new SimpleShipDisplayInfo<Character>('a', 'A');
    assertEquals('A', shipDisplayInfo.getInfo(new Coordinate(1, 1), true));
    assertEquals('a', shipDisplayInfo.getInfo(new Coordinate(1, 1), false));
  }

}
