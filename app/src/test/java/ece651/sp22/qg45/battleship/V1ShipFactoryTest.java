package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
  private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter,
      Coordinate... expectedLocs) {
    assertEquals(expectedName, testShip.getName());
    for (Coordinate c : expectedLocs) {
      assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
    }
  }

  @Test
  public void test_make_submarine() {
    V1ShipFactory f = new V1ShipFactory();
    Placement where = new Placement(new Coordinate(1, 2), 'V');
    checkShip(f.makeSubmarine(where), "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));
    Placement where_ = new Placement(new Coordinate(1, 2), 'H');
    checkShip(f.makeSubmarine(where_), "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));
  }

  @Test
  public void test_make_bs() {
    V1ShipFactory f = new V1ShipFactory();
    Placement where = new Placement(new Coordinate(1, 2), 'V');
    checkShip(f.makeBattleship(where), "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2),
        new Coordinate(3, 2), new Coordinate(4, 2));
    Placement where_ = new Placement(new Coordinate(1, 2), 'H');
    checkShip(f.makeBattleship(where_), "Battleship", 'b', new Coordinate(1, 2), new Coordinate(1, 3),
        new Coordinate(1, 4), new Coordinate(1, 5));
  }

  @Test
  public void test_make_c() {
    V1ShipFactory f = new V1ShipFactory();
    Placement where = new Placement(new Coordinate(1, 2), 'V');
    checkShip(f.makeCarrier(where), "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2),
        new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));
    Placement where_ = new Placement(new Coordinate(1, 2), 'H');
    checkShip(f.makeCarrier(where_), "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
        new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));
  }

  @Test
  public void test_make_d() {
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    Placement v1_2_ = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst_ = f.makeDestroyer(v1_2_);
    checkShip(dst_, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
  }
}
