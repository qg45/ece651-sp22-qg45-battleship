package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

// import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }

  @Test
  void test_read_placement() throws IOException {
    // StringReader sr = new StringReader("B2V\nC8H\na4v\n");
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    // PrintStream ps = new PrintStream(bytes, true);
    // Board<Character> b = new BattleShipBoard<Character>(10, 20);
    // App app = new App(b, sr, ps);
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
    String prompt = "Player A where do you want to place a Destroyer?";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');
    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]); // did we get the right Placement back
      assertEquals(prompt + "\n", bytes.toString()); // should have printed prompt and newline
      bytes.reset(); // clear out bytes for next time around
    }
  }

  @Test
  void test_do_one_palcement() throws IOException {
    // StringReader sr = new StringReader("A0H");
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    // PrintStream ps = new PrintStream(bytes, true);
    // Board<Character> b = new BattleShipBoard<Character>(3, 2);
    // App app = new App(b, sr, ps);
    // app.doOnePlacement();
    TextPlayer player = createTextPlayer(3, 2, "A0H", bytes);
    // player.doOnePlacement();
    String shipName = "Destroyer";
    player.doOnePlacement(shipName, player.shipCreationFns.get(shipName));
    String expectedHead = "  0|1|2\n";
    String expectedBody = "A d|d|d A\n" + "B  | |  B\n";
    String expected = expectedHead + expectedBody + expectedHead;
    assertEquals(expected, player.view.displayMyOwnBoard());
  }

  // @Disabled
  // @Test
  // void test_placement_phase() throws IOException {
  //   ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  //   TextPlayer player = createTextPlayer(3, 2, "A0H", bytes);
  //   player.doPlacementPhase();
  //   String expectedHead = "  0|1|2\n";
  //   String emptyBody = "A  | |  A\n" + "B  | |  B\n";
  //   String expectedBody = "A d|d|d A\n" + "B  | |  B\n";
  //   String instruction = "Player " + player.name + ": you are going to place the following ships (which are all\n" +
  //       "rectangular). For each ship, type the coordinate of the upper left\n" +
  //       "side of the ship, followed by either H (for horizontal) or V (for\n" +
  //       "vertical).  For example M4H would place a ship horizontally starting\n" +
  //       "at M4 and going to the right.  You have\n\n" +
  //       "2 \"Submarines\" ships that are 1x2\n" +
  //       "3 \"Destroyers\" that are 1x3\n" +
  //       "3 \"Battleships\" that are TShaped\n" +
  //       "2 \"Carriers\" that are ZShaped\n";
  //   String prompt = "Player A where do you want to place a Destroyer?";
  //   String emptyBoard = expectedHead + emptyBody + expectedHead;
  //   // String expected = emptyBoard + instruction + prompt + "\n" + expectedHead + expectedBody + expectedHead;
  //   // assertEquals(expected, bytes.toString());
  //   String expected = expectedHead + expectedBody + expectedHead;
  //   assertEquals(expected, player.view.displayMyOwnBoard());
  // }
}
