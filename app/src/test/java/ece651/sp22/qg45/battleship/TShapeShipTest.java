package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class TShapeShipTest {
  @Test
  public void test_constructor() {
    Ship<Character> ship = new TShapedShip<Character>("Battleship", new Coordinate(0, 0), 'R', 'b', '*');
    assertEquals("Battleship", ship.getName());
    HashSet<Coordinate> set = new HashSet<>();
    set.add(new Coordinate(0, 0));
    set.add(new Coordinate(1, 0));
    set.add(new Coordinate(1, 1));
    set.add(new Coordinate(2, 0));
    for (Coordinate c : ship.getCoordinates()) {
      assertTrue(set.contains(c));
    }
    assertEquals(new Coordinate(0, 0), ship.getPivot());
    Ship<Character> ship1 = new TShapedShip<Character>("Battleship", new Coordinate(0, 0), 'U', 'b', '*');
    Ship<Character> ship2 = new TShapedShip<Character>("Battleship", new Coordinate(0, 0), 'L', 'b', '*');
    Ship<Character> ship3 = new TShapedShip<Character>("Battleship", new Coordinate(0, 0), 'D', 'b', '*');
    assertEquals("Battleship", ship1.getName());
    assertEquals("Battleship", ship2.getName());
    assertEquals("Battleship", ship3.getName());
  }

}
