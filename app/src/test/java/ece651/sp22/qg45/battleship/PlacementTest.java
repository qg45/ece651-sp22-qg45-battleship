package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_basic_constructor() {
    Placement p = new Placement(new Coordinate("B1"), 'V');
    assertEquals(new Coordinate("B1"), p.getCoordinate());
    assertEquals('V', p.getOrientation());
    Coordinate c1 = new Coordinate("B1");
    Coordinate c2 = new Coordinate("b1");
    Placement p1 = new Placement(c1, 'v');
    Placement p2 = new Placement(c2, 'V');
    assertEquals(p1, p2);
  }

  @Test
  public void test_string_constructor() {
    Placement p = new Placement("B1V");
    assertEquals(new Coordinate("B1"), p.getCoordinate());
    assertEquals('V', p.getOrientation());
  }

  @Test
  public void test_invalid_input() {
    Coordinate c = new Coordinate(1, 1);
    assertThrows(IllegalArgumentException.class, () -> new Placement(c, 'o'));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A1Vh"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A1o"));
  }

  @Test
  public void test_equal() {
    Placement p = new Placement("G1H");
    Placement p2 = new Placement("g1h");
    Placement p3 = new Placement("Z2v");
    Placement p4 = new Placement("G1v");
    assertEquals(p, p);
    assertEquals(p, p2);
    assertEquals(p2, p);
    assertNotEquals(p3, p);
    assertNotEquals(null, p);
    assertNotEquals(p, null);
    assertNotEquals(p, "G1H");
    assertNotEquals(p, p4);
  }

  @Test
  public void test_hash() {
    Placement p = new Placement("G1H");
    Placement p2 = new Placement("g1h");
    Placement p3 = new Placement("Z2v");

    assertEquals(p.hashCode(), p2.hashCode());
    assertNotEquals(p.hashCode(), p3.hashCode());
  }
}
