package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_make_coor() {
    HashSet<Coordinate> s1 = RectangleShip.makeCoords(new Coordinate(1, 2), 1, 3);
    assertTrue(s1.contains(new Coordinate(1, 2)));
    assertTrue(s1.contains(new Coordinate(2, 2)));
    assertTrue(s1.contains(new Coordinate(3, 2)));

    HashSet<Coordinate>s2 = RectangleShip.makeCoords(new Coordinate(1, 2), 3, 1);
    assertTrue(s2.contains(new Coordinate(1, 2)));
    assertTrue(s2.contains(new Coordinate(1, 3)));
    assertTrue(s2.contains(new Coordinate(1, 4)));

    HashSet<Coordinate>s3 = RectangleShip.makeCoords(new Coordinate(0, 0), 2, 2);
    assertTrue(s3.contains(new Coordinate(0, 0)));
    assertTrue(s3.contains(new Coordinate(0, 1)));
    assertTrue(s3.contains(new Coordinate(1, 0)));
    assertTrue(s3.contains(new Coordinate(1, 1)));
  }

  @Test
  public void test_constructor() {
    RectangleShip<Character> rectangleShip = new RectangleShip<>("theShip", new Coordinate(0, 0), 2, 3, 's', '*');
    assertTrue(rectangleShip.occupiesCoordinates(new Coordinate(2, 1)));
    assertFalse(rectangleShip.occupiesCoordinates(new Coordinate(3,1)));
  }

  @Test
  public void test_coor_check() {
    RectangleShip<Character> rectangleShip = new RectangleShip<Character>("theShip", new Coordinate(0, 0), 1, 3, 's', '*');
    assertThrows(IllegalArgumentException.class, () -> rectangleShip.checkCoordinateInThisShip(new Coordinate(0, 1)));
    assertThrows(IllegalArgumentException.class, () -> rectangleShip.checkCoordinateInThisShip(new Coordinate(3, 0)));
  }

  @Test
  public void test_hit() {
    RectangleShip<Character> rectangleShip = new RectangleShip<Character>("theShip", new Coordinate(0, 0), 1, 3, 's', '*');
    rectangleShip.checkCoordinateInThisShip(new Coordinate(1, 0));
    assertFalse(rectangleShip.isSunk());
    assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(1, 0), true)); 
    assertEquals(null, rectangleShip.getDisplayInfoAt(new Coordinate(1, 0), false)); 
    assertFalse(rectangleShip.wasHitAt(new Coordinate(1, 0)));
    rectangleShip.recordHitAt(new Coordinate(1, 0));
    assertEquals('*', rectangleShip.getDisplayInfoAt(new Coordinate(1, 0), true));
    assertEquals('s', rectangleShip.getDisplayInfoAt(new Coordinate(1, 0), false));
    assertFalse(rectangleShip.isSunk());
    assertTrue(rectangleShip.wasHitAt(new Coordinate(1, 0)));
    for (Coordinate c : rectangleShip.getHittedParts()) {
      assertEquals('*', rectangleShip.getDisplayInfoAt(c, true));
    }
  }

  @Test
  public void test_get_name() {
    RectangleShip<Character> rectangleShip = new RectangleShip<Character>("theShip", new Coordinate(0, 0), 1, 3, 's', '*');
    assertEquals("theShip", rectangleShip.getName());
    assertNotEquals("Theship", rectangleShip.getName());
  }

  @Test
  public void test_get_coor() {
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'v'));
    Set<Coordinate> set = new HashSet<>();
    set.add(new Coordinate(0, 0));
    set.add(new Coordinate(1, 0));
    for (Coordinate c : s.getCoordinates()) {
      assertTrue(set.contains(c));
    }
  }
}
