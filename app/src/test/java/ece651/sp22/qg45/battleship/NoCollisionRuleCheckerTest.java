package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_no_coll() {
    V1ShipFactory f = new V1ShipFactory();
    PlacementRuleChecker<Character> checker = new InBoundsRuleChecker<>(new NoCollisionRuleChecker<>(null));
    Board<Character> b = new BattleShipBoard<>(3, 3, 'X');
    Ship<Character> s1 = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'v'));
    Ship<Character> s2 = f.makeSubmarine(new Placement(new Coordinate(0, 0), 'H'));
    Ship<Character> s3 = f.makeDestroyer(new Placement(new Coordinate(1, 0), 'H'));
    assertNull(checker.checkPlacement(s1, b));
    b.tryAddShip(s1);
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkPlacement(s2, b));
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkPlacement(s3, b));
    Ship<Character> s4 = f.makeDestroyer(new Placement(new Coordinate(2, 0), 'H'));
    assertNull(checker.checkPlacement(s4, b));
  }

}
