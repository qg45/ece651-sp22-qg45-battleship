package ece651.sp22.qg45.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {

  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    // assertEquals(expectedBody, view.makeRows((c)->view.toDisplay.whatIsAtForSelf(c)));
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_display_empty_2by2() {
    String expectedHeader = "  0|1\n";
    String expectedBody = "A  |  A\n" + "B  |  B\n";
    emptyBoardHelper(2, 2, expectedHeader, expectedBody);
  }

  @Test
  public void test_display_empty_3by2() {
    String expectedHeader = "  0|1|2\n";
    String expectedBody = "A  | |  A\n" + "B  | |  B\n";
    emptyBoardHelper(3, 2, expectedHeader, expectedBody);
  }

  @Test
  public void test_display_empty_3by5() {
    String expectedHeader = "  0|1|2\n";
    String expectedBody = "A  | |  A\n" + "B  | |  B\n";
    expectedBody += "C  | |  C\n" + "D  | |  D\n" + "E  | |  E\n";
    emptyBoardHelper(3, 5, expectedHeader, expectedBody);
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27, 'X');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

  @Test
  public void test_board_with_ship() {
    Board<Character> b = new BattleShipBoard<Character>(4, 3, 'X');
    Ship<Character> s1 = new RectangleShip<Character>(new Coordinate(0, 0), 's', '*');
    Ship<Character> s2 = new RectangleShip<Character>(new Coordinate(1, 1), 's', '*');
    b.tryAddShip(s1);
    b.tryAddShip(s2);
    BoardTextView view = new BoardTextView(b);
    String expectedHeader = "  0|1|2|3\n";
    String expectedBody = "A s| | |  A\n" + "B  |s| |  B\n" + "C  | | |  C\n";
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
    // enemy part
    String expectedBody1 = "A  | | |  A\n" + "B  | | |  B\n" + "C  | | |  C\n";
    String expected1 = expectedHeader + expectedBody1 + expectedHeader;
    assertEquals(expected1, view.displayEnemyBoard());
    b.fireAt(new Coordinate(0, 1));
    String expectedBody2 = "A  |X| |  A\n" + "B  | | |  B\n" + "C  | | |  C\n";
    String expected2 = expectedHeader + expectedBody2 + expectedHeader;
    assertEquals(expected2, view.displayEnemyBoard());
    b.fireAt(new Coordinate(0, 0));
    String expectedBody3 = "A s|X| |  A\n" + "B  | | |  B\n" + "C  | | |  C\n";
    String expected3 = expectedHeader + expectedBody3 + expectedHeader;
    assertEquals(expected3, view.displayEnemyBoard());
    String expectedBody4 = "A *| | |  A\n" + "B  |s| |  B\n" + "C  | | |  C\n";
    String expected4 = expectedHeader + expectedBody4 + expectedHeader;
    assertEquals(expected4, view.displayMyOwnBoard());
  }
}
